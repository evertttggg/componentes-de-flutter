import 'dart:math';

import 'package:flutter/material.dart';

class AnimatedContainerPage extends StatefulWidget {
  @override
  _AnimatedContainerPageState createState() => _AnimatedContainerPageState();
}

class _AnimatedContainerPageState extends State<AnimatedContainerPage> {
  double _width = 50.0;
  double _heigth = 50.0;
  Color _color = Colors.pink;
  BorderRadiusGeometry _borderRadios = BorderRadius.circular(8.0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Animation App'),
        centerTitle: true,
      ),
      body: Center(
        child: AnimatedContainer(
          curve: Curves.bounceIn,
          duration: Duration(seconds: 1),
          width: _width,
          height: _heigth,
          decoration: BoxDecoration(
              color: _color,
              borderRadius: _borderRadios,
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.black12,
                    blurRadius: 5.0,
                    spreadRadius: 5.0,
                    offset: Offset(2.0, 10.0))
              ]),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.play_circle_outline,
        ),
        onPressed: _cambiarForma,
      ),
    );
  }

  void _cambiarForma() {
    final _random = Random();

    setState(() {
      _heigth = _random.nextInt(300).toDouble();

      _width = _random.nextInt(300).toDouble();

      _color = Color.fromRGBO(
          _random.nextInt(255), _random.nextInt(255), _random.nextInt(255), 1);

      _borderRadios = BorderRadius.circular(_random.nextInt(100).toDouble());
    });
  }
}
