import 'package:flutter/material.dart';

class InputPage extends StatefulWidget {
  InputPage({Key key}) : super(key: key);

  @override
  _InputPageState createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  String _nombre = '';
  String _email = '';
  String _fecha = '';
  String _optSelection = 'Uno';

  List<String> _opciones = ['Uno', 'Dos', 'Tres'];

  TextEditingController _inputText = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Input App'),
        centerTitle: true,
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
        children: <Widget>[
          _crearInput(),
          Divider(),
          _crearEmail(),
          Divider(),
          _crearPassword(),
          Divider(),
          _crearFecha(context),
          Divider(),
          _crearDropdown(),
          Divider(),
          _crearPersona()
        ],
      ),
    );
  }

  Widget _crearInput() {
    return TextField(
      // autofocus: true,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
        counter: Text('Letras ${_nombre.length}'),
        labelText: 'Nombre',
        icon: Icon(Icons.account_circle),
        helperText: 'Solo es el nombre',
        suffixIcon: Icon(Icons.accessibility),
        hintText: 'Nombre de la Persona',
      ),
      onChanged: (valor) {
        setState(() {
          _nombre = valor;
        });
      },
    );
  }

  Widget _crearEmail() {
    return TextField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          labelText: 'Email',
          hintText: 'Email',
          icon: Icon(Icons.email),
          suffixIcon: Icon(Icons.alternate_email),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(20.0))),
      onChanged: (valor) => setState(() {
        _email = valor;
      }),
    );
  }

  Widget _crearPassword() {
    return TextField(
      obscureText: true,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
        icon: Icon(Icons.lock_open),
        labelText: 'Password',
        hintText: 'Password',
      ),
    );
  }

  Widget _crearFecha(BuildContext context) {
    return TextField(
      controller: _inputText,
      enableInteractiveSelection: false,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          labelText: 'Fecha de Nacimiento',
          icon: Icon(Icons.perm_contact_calendar),
          hintText: 'Fecha de Nacimiento',
          suffixIcon: Icon(Icons.calendar_today)),
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
        _crearDate(context);
      },
    );
  }

  _crearDate(BuildContext context) async {
    DateTime piked = await showDatePicker(
        context: context,
        initialDate: new DateTime.now(),
        firstDate: new DateTime(2019),
        lastDate: new DateTime(2021),
        locale: Locale('es', 'ES'));

    if (piked != null) {
      setState(() {
        _fecha = piked.toString();
        _inputText.text = _fecha;
      });
    }
  }

  List<DropdownMenuItem<String>> escogerOpciones() {
    List<DropdownMenuItem<String>> lista = new List();
    _opciones.forEach((opciones) {
      lista.add(DropdownMenuItem(
        child: Text(opciones),
        value: opciones,
      ));
    });
    return lista;
  }

  Widget _crearDropdown() {
    return Row(
      children: <Widget>[
        Icon(Icons.select_all),
        SizedBox(width: 30.0),
        DropdownButton(
          value: _optSelection,
          items: escogerOpciones(),
          onChanged: (opt) {
            setState(() {
              _optSelection = opt;
            });
          },
        ),
      ],
    );
  }

  Widget _crearPersona() {
    return ListTile(
      title: Text('Nombre es: $_nombre'),
      subtitle: Text('Email: $_email'),
    );
  }
}
