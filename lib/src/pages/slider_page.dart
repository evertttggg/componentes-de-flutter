import 'package:flutter/material.dart';

class SliderPage extends StatefulWidget {
  @override
  _SliderPageState createState() => _SliderPageState();
}

class _SliderPageState extends State<SliderPage> {
  double _valorPorDefecto = 100.0;
  bool _estadoDelSlider = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Slider App'),
        centerTitle: true,
      ),
      body: Container(
        padding: EdgeInsets.only(top: 50.0),
        child: Column(
          children: <Widget>[
            _crearSlider(),
            _checkBox(),
            _crearSwitch(),
            Expanded(child: _crearImg()),
          ],
        ),
      ),
    );
  }

  Widget _crearSlider() {
    return Slider(
      activeColor: Colors.indigoAccent,
      label: 'Tameño de la Imagen',
      //  divisions: 20,
      value: _valorPorDefecto,
      max: 400.0,
      min: 10.0,
      onChanged: (_estadoDelSlider)
          ? null
          : (valor) {
              setState(() {
                _valorPorDefecto = valor;
              });
            },
    );
  }

  Widget _checkBox() {
    // return Checkbox(
    //   value: _estadoDelSlider,
    //   onChanged: (valor) {
    //     setState(() {
    //       _estadoDelSlider = valor;
    //     });
    //   },
    // );
    return CheckboxListTile(
      title: Text('Bloquear Slider :'),
      value: _estadoDelSlider,
      onChanged: (valor) {
        setState(() {
          _estadoDelSlider = valor;
        });
      },
    );
  }

  Widget _crearSwitch() {
    return SwitchListTile(
      title: Text('Bloquear Slider :'),
      value: _estadoDelSlider,
      onChanged: (valor) {
        setState(() {
          _estadoDelSlider = valor;
        });
      },
    );
  }

  Widget _crearImg() {
    return Image(
      image: NetworkImage(
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSSmEYDFv3-oe8TUE264Zt9JQgj-huPFza6FuGKSRML57Z61Cj5_g&s'),
      width: _valorPorDefecto,
      fit: BoxFit.contain,
    );
  }
}
