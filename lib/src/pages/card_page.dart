import 'package:flutter/material.dart';

class CardPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Card App'),
        centerTitle: true,
      ),
      body: ListView(
        padding: EdgeInsets.all(15),
        children: <Widget>[
          _card1(),
          SizedBox(
            height: 30.0,
          ),
          _cart2(),
          SizedBox(
            height: 30.0,
          ),
          _card1(),
          SizedBox(
            height: 30.0,
          ),
          _cart2(),
          SizedBox(
            height: 30.0,
          ),
          _card1(),
          SizedBox(
            height: 30.0,
          ),
          _cart2(),
          SizedBox(
            height: 30.0,
          ),
          _card1(),
          SizedBox(
            height: 30.0,
          ),
          _cart2(),
        ],
      ),
    );
  }

  Widget _card1() {
    return Card(
      elevation: 23.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(17.0)),
      child: Column(
        children: <Widget>[
          ListTile(
            leading: Icon(
              Icons.art_track,
              color: Colors.blue,
            ),
            title: Text(
              'Soy el titulo',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
                'Soy la discripcion de esta pagina que esta aca, puedes conseguir mas informacion aqui sobre la card y can puedes cancelar y hacer muchas cosas mas aqui'),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FlatButton(
                onPressed: () {},
                child: Text('OK'),
              ),
              FlatButton(
                onPressed: () {},
                child: Text('Cancelar'),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget _cart2() {
    final card = Container(
      // clipBehavior: Clip.antiAlias,
      child: Column(
        children: <Widget>[
          FadeInImage(
            image: NetworkImage('url'),
            placeholder: AssetImage('assets/jar-loading.gif'),
            fadeInDuration: Duration(milliseconds: 200),
            height: 300,
            fit: BoxFit.cover,
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Text('No tengo Idea de que poner...'),
          )
        ],
      ),
    );
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30.0),
          color: Colors.white,
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.black26,
                blurRadius: 4.0,
                spreadRadius: 17.0,
                offset: Offset(2.0, 10.0))
          ]),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(30.0),
        child: card,
      ),
    );
  }
}
