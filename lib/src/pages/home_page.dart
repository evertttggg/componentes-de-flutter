// import 'dart:js';

// import 'package:componentes/src/pages/alert_page.dart';
import 'package:componentes/src/providers/menu_provider.dart';
import 'package:componentes/src/utils/icon_string_utils.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Component App'),
        centerTitle: true,
      ),
      body: _lista(),
    );
  }

  Widget _lista() {
    return FutureBuilder(
      future: menuProvider.cargarData(),
      initialData: [],
      builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
        return ListView(
          children: _listItems(snapshot.data, context),
        );
      },
    );
  }

  List<Widget> _listItems(List<dynamic> data, BuildContext context) {
    final List<Widget> opciones = [];

    // if (data == null) {
    //   return [];
    // }

    data.forEach((opt) {
      final wigdgetTem = ListTile(
        title: Text(opt['texto']),
        leading: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              CircleAvatar(
                  radius: 20,
                  backgroundColor: Colors.blueGrey,
                  child: getIcon(opt['icon']))
            ],
          ),
        ),
        trailing: Icon(Icons.navigate_next),
        onTap: () {
          Navigator.pushNamed(context, opt['ruta']);
          // final route = MaterialPageRoute(
          //   builder: (context) {
          //     return AlertPage();
          //   },
          // );
          // Navigator.push(context, route);
        },
      );
      opciones..add(wigdgetTem)..add(Divider());
    });
    return opciones;
  }
}
