import 'package:flutter/material.dart';

class HomeTems extends StatelessWidget {
  final lista = ['Uno', 'Dos', 'Tres', 'Cuatro', 'Cinco'];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Components Tems App'),
        centerTitle: true,
      ),
      body: ListView(children: _listas()),
    );
  }

  List<Widget> _listas() {
    return lista.map((item) {
      return Column(
        children: <Widget>[
          ListTile(
            leading: Column(
              children: <Widget>[
                CircleAvatar(
                  backgroundColor: Colors.blueGrey,
                  radius: 20,
                  child: Icon(Icons.layers),
                )
              ],
            ),
            title: Text(item + '!!'),
            subtitle: Text('Mas Cosas........'),
            trailing: Icon(Icons.keyboard_arrow_right),
            onTap: () {},
          ),
          Divider()
        ],
      );
    }).toList();
  }
}
